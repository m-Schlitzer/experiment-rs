use cursive::event::Key;
use cursive::menu::MenuTree;
use cursive::theme::{Color, PaletteColor};
use cursive::traits::*;
use cursive::views::{
    Button, Checkbox, Dialog, DummyView, EditView, LinearLayout, ListView, SelectView,
};
use cursive::Cursive;
use std::env;

fn main() {
    let mut siv = Cursive::default();
    siv.menubar()
        .add_subtree(
            "Application",
            MenuTree::new()
                .leaf("Welcome Screen", |mut s| show_welcome_screen(&mut s))
                .leaf("Name List", |mut s| start_name_list(&mut s))
                .leaf("Stash", |mut s| start_inventory(&mut s))
                .leaf("Are you root?", |mut s| are_you_root(&mut s)),
        )
        .add_subtree(
            "Help",
            MenuTree::new().subtree(
                "Not",
                MenuTree::new().subtree(
                    "For",
                    MenuTree::new().leaf("You", |s| s.add_layer(Dialog::info("Too bad!"))),
                ),
            ),
        )
        .add_leaf("Quit", |s| {
            quit_prompt(s);
        });
    siv.set_autohide_menu(false);
    siv.add_global_callback(Key::Esc, |s| s.select_menubar());

    let mut theme = siv.current_theme().clone();
    theme.palette[PaletteColor::Background] = Color::TerminalDefault;
    siv.set_theme(theme);
    show_welcome_screen(&mut siv);
    siv.run();
}

fn are_you_root(s: &mut Cursive) {
    let mut root = "No!";
    if check_if_root() {
        root = "Yes!";
    }

    s.add_layer(Dialog::info(root).title("Are you root?"))
}

fn check_if_root() -> bool {
    match env::var("USER") {
        Ok(val) => val == "root",
        Err(_) => false,
    }
}

fn quit_prompt(s: &mut Cursive) {
    s.add_layer(
        Dialog::text("Are you sure?")
            .title("Quitting")
            .button("Yes", |s| {
                s.quit();
            })
            .button("No", |s| {
                s.pop_layer();
            }),
    )
}

fn show_welcome_screen(s: &mut Cursive) {
    s.pop_layer();
    let emoji = "MMMMMMMMMMMMMMMMMMMMMMMMMMNdyo+/:..............:/+oydNMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNhs/-............................-/shNMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMhs:......................................:shMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMdo-............................................-odMMMMMMMMMMMMMMM
MMMMMMMMMMMMNy-..................................................-yNMMMMMMMMMMMM
MMMMMMMMMMmo-......................................................-omMMMMMMMMMM
MMMMMMMMMo............................................................oMMMMMMMMM
MMMMMMMh-..............................................................-hMMMMMMM
MMMMMN+..................................................................+NMMMMM
MMMMm:....................................................................:mMMMM
MMMm-......................................................................-mMMM
MMm:........................................................................-mMM
MN-..........................................................................-NM
Mo.................-/+/--.............................-:/oo/-.................oM
h..................:ddddhs+-.......................-/shddddy-..................h
:..................`.-/ohdddy/-..................:sdddho:.````.................:
...............`         .+hddy:...............-odddo-          `...............
.............`              /dddo-............:hdds.     `.`      `.............
............`    `ohddho`    .hddo...........-hddo     /hdddho`    .............
............    `yddddddh`    -yds-..........-sy+`    /dddddddo    `............
............    `hddddddh`    ..-................`    :ddddddd+    `............
............`    .sdddds.    `....................     -shdhs:     .............
..........-//.      ``      `......................`             `:/:-..........
........-:////:.          `..........................`         `-/////:-........
:.....-://///////:-....+shdddhyyyssssoooooossssyyyhddddyo----://////////:-.....:
h....-/////////////////yddddddddddddddddddddddddddddddddy////////////////:-....h
Mo..-//////////////////- `.-:://+++oooooooooo+++//::-.``://////////////////-..oM
MN::////////////////////`                              `////////////////////::NM
Mm//////////////////////yy/-                        -/yy//////////////////////NM
Mh//////////////////////yddddyo+:-.``      ``.-:+oyddddy//////////////////////hM
Mh//////////////////////yddddddddddddddddddddddddddddddy//////////////////////hM
Mm/////////////////////+dddddddddddddddddddddddddddddddd//////////////////////NM
MMy////////////////////oddddddddddddddddddddddddddddddddo////////////////////hMM
MMMh+////////////////:-.-+yddddddddddddddddddddddddddy+-.-:////////////////+dMMM
MMMMMh+////////////:-......-/+shddddddddddddddddhs+/-......-:////////////+hMMMMM
MMMMMMMNdyysoo/:--..............-:/++oooooo++/:-..............--:+oosyhdNMMMMMMM
MMMMMMMMMMMMMMMdo-............................................-odMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMhs:......................................:shMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNhs/-............................-/shNMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMNdyo+/:..............:/+oydNMMMMMMMMMMMMMMMMMMMMMMMMMM";

    s.add_layer(Dialog::text(emoji).title("Welcome").button("Close", |s| {
        s.pop_layer();
    }));
}

fn start_inventory(s: &mut Cursive) {
    s.pop_layer();
    s.add_layer(
        Dialog::text("Stash: Inventory Management System")
            .title("Stash")
            .button("New Stash", |s| {
                create_new_stash(s);
            })
            .button("Open Stash", |s| {
                s.pop_layer();
            })
            .button("Close", |s| {
                s.pop_layer();
            }),
    );
}

fn create_new_stash(s: &mut Cursive) {
    s.pop_layer();
    s.add_layer(
        Dialog::around(
            ListView::new()
                .child("Stash Name: ", EditView::new().fixed_width(15))
                .child(
                    "Enable Location",
                    Checkbox::new().on_change(|s, checked| {
                        s.call_on_id("location", |view: &mut EditView| view.set_enabled(checked));
                        if checked {
                            s.focus_id("location").unwrap();
                        }
                    }),
                )
                .child(
                    "Location",
                    EditView::new()
                        .disabled()
                        .with_id("location")
                        .fixed_width(15),
                )
                .child("Other Thing: ", EditView::new().fixed_width(15)),
        )
        .title("Stash")
        .button("Confirm", |s| {
            s.pop_layer();
        })
        .button("Cancel", |s| {
            start_inventory(s);
        }),
    )
}

fn start_name_list(s: &mut Cursive) {
    s.pop_layer();
    let select = SelectView::<String>::new()
        .on_submit(on_submit)
        .with_id("select")
        .fixed_size((20, 10));

    let buttons = LinearLayout::vertical()
        .child(Button::new("Add new", add_name))
        .child(Button::new("Delete", delete_name))
        .child(DummyView)
        .child(Button::new("Quit", |s| {
            s.pop_layer();
        }));

    s.add_layer(
        Dialog::around(
            LinearLayout::horizontal()
                .child(select)
                .child(DummyView)
                .child(buttons),
        )
        .title("Select a profile"),
    );
}

fn on_submit(s: &mut Cursive, name: &String) {
    s.add_layer(
        Dialog::text(format!("Name: {}\n Seems Good.", name))
            .title(format!("{}'s Info", name))
            .button("Go back", |s| {
                s.pop_layer();
            }),
    );
}

fn add_name(s: &mut Cursive) {
    fn ok(s: &mut Cursive, name: &str) {
        s.call_on_id("select", |view: &mut SelectView<String>| {
            view.add_item_str(name);
        });
        s.pop_layer();
    }

    s.add_layer(
        Dialog::around(
            EditView::new()
                .on_submit(ok)
                .with_id("name")
                .fixed_width(10),
        )
        .title("Enter a new name")
        .button("Ok", |s| {
            let name = s
                .call_on_id("name", |view: &mut EditView| view.get_content())
                .unwrap();
            ok(s, &name);
        })
        .button("Cancel", |s| {
            s.pop_layer();
        }),
    );
}

fn delete_name(s: &mut Cursive) {
    let mut select = s.find_id::<SelectView<String>>("select").unwrap();
    match select.selected_id() {
        None => s.add_layer(Dialog::info("No name to remove")),
        Some(focus) => {
            select.remove_item(focus);
        }
    }
}
